/**
 * main.ts
 *
 * Bootstraps Vuetify and other plugins then mounts the App`
 */

// Plugins
import { registerPlugins } from '@/plugins'

// Components
import App from './App.vue'

// Composables
import { createApp } from 'vue'
import {createRouter, createWebHistory, Router, RouterHistory, RouterOptions} from 'vue-router'


const app = createApp(App)

registerPlugins(app)

import Dashboard from "@/pages/Dashboard.vue";

const router: Router = createRouter({
  history: createWebHistory(),
  routes: [
    { path: '/', component: Dashboard }
  ]
} as RouterOptions)

app.use(router)
app.mount('#app')
